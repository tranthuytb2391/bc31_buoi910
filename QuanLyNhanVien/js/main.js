var mangNhanVien = [];
// ----------------THÊM NHÂN VIÊN MỚI-------------------
//Khi người dùng click vào nút btnThemNV thì bắt đầu lấy thông tin người dùng để lưu trữ
document.getElementById("btnThemNV").onclick = function() {
    var tk = document.getElementById("tknv").value;
    var hoTen = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var mk = document.getElementById("password").value;
    var ngayLam = document.getElementById("datepicker").value;
    var luongCoBan = document.getElementById("luongCB").value * 1;
    var chucVu = chucvu.options[chucvu.selectedIndex].text;
    var gioLam = document.getElementById("gioLam").value * 1;

    var nhanVien = new NhanVien(tk, hoTen, email, mk, ngayLam, luongCoBan, chucVu, gioLam);

    var valid = true;
    //Kiểm tra rỗng


    valid &= kiemTraRong(nhanVien.tk, "#tbTKNV", "Tài khoản") & kiemTraRong(nhanVien.hoTen, "#tbTen", "Họ và tên") & kiemTraRong(nhanVien.email, "#tbEmail", "Email") & kiemTraRong(nhanVien.mk, "#tbMatKhau", "Mật khẩu") & kiemTraRong(nhanVien.ngayLam, "#tbNgay", "Ngày làm") & kiemTraRong(nhanVien.luongCoBan, "#tbLuongCB", "Lương cơ bản") & kiemTraRong(nhanVien.gioLam, "#tbGiolam", "Giờ làm");


    if (kiemTraRong(nhanVien.tk, "#tbTKNV", "Tài khoản")) {
        valid &= kiemTraDoDai(nhanVien.tk, '#tbTKNV', 'Tài khoản', 4, 6);
    }

    if (kiemTraRong(nhanVien.hoTen, "#tbTen", "Họ và tên")) {
        valid &= kiemTraTatCaKyTu(nhanVien.hoTen, "#tbTen", "Họ và tên");
    }

    if (kiemTraRong(nhanVien.email, "#tbEmail", "Email")) {
        valid &= kiemTraEmail(nhanVien.email, "#tbEmail", "Email");
    }

    if (kiemTraRong(nhanVien.mk, "#tbMatKhau", "Mật khẩu")) {
        valid &= kiemTraMatKhau(nhanVien.mk, "#tbMatKhau", "Mật khẩu");
    }

    if (kiemTraRong(nhanVien.ngayLam, "#tbNgay", "Ngày làm")) {
        valid &= kemTraDinhDangNgay(nhanVien.ngayLam, "#tbNgay", "Ngày làm");
    }

    if (kiemTraRong(nhanVien.luongCoBan, "#tbLuongCB", "Lương cơ bản")) {
        valid &= kiemTraGiaTri(nhanVien.luongCoBan, "#tbLuongCB", "Lương cơ bản", 1000000, 20000000);
    }


    if (kiemTraRong(nhanVien.gioLam, "#tbGiolam", "Giờ làm")) {
        valid &= kiemTraGiaTri(nhanVien.gioLam, "#tbGiolam", "Giờ làm", 80, 200);
    }

    if (!valid) {
        return;
    }

    //Mỗi lần sau khi nhập liệu và bấm nút xác nhận thì thêm nhân viên vào mảng
    mangNhanVien.push(nhanVien);

    //Lưu vào localstorage
    setStore(mangNhanVien, 'mangNhanVien');

    //Gọi hàm tạo table nhanVien
    var html = renderNhanVien(mangNhanVien);
    document.getElementById("tableDanhSach").innerHTML = html;
};


// --------in ra table danh sách nhân viên-------------
/**
 * Hàm này sẽ nhận vào 1 array (nhanVien) và trả ra output là string <tr>....</tr>
 * @param {*} arrNhanVien arrNhanVien là mảng các object nhanVien [nhanVien1,nhanVien2,...]
 * @returns trả ra 1 giá trị là 1 htmlString '<tr>...</tr> <tr>...</tr>'
 */

function renderNhanVien(arrNhanVien) { //param : input :arrNhanVien
    var html = ""; //output: string html 
    for (var i = 0; i < arrNhanVien.length; i++) {
        var item = arrNhanVien[i]; //Mỗi lần duyệt lấy ra 1 object nhanVien từ mảng
        var nv = new NhanVien(
            item.tk,
            item.hoTen,
            item.email,
            item.mk,
            item.ngayLam,
            item.luongCoBan,
            item.chucVu,
            item.gioLam);
        html += `
            <tr>
                <td>${nv.tk}</td>
                <td>${nv.hoTen}</td>
                <td>${nv.email}</td>
                <td>${nv.ngayLam}</td>
                <td>${nv.chucVu}</td>
                <td>${nv.tongLuong()}</td>
                <td>${nv.xepLoai()}</td>

                <td>
                    <button class="btn btn-danger" onclick="xoaNhanVien(${nv.tk})">Xoá</button>
                    <button class="btn btn-primary">Sửa</button>
                </td>
            </tr>
        `;
    }
    return html;
};

/**
 * Hàm nhận vào 1 giá trị object hoặc array lưu vào localStorage của trình duyệt
 * @param {*} content là object hoặc array muốn lưu
 * @param {*} storeName là tên của storage
 */

function setStore(content, storeName) {
    var sContent = JSON.stringify(content); //Biến đổi object hoặc array thành chuỗi
    localStorage.setItem(storeName, sContent); //Đem chuỗi đó lưu vào localstorage
}
/**
 * Hàm lấy dữ liệu từ localstorage dựa vào storeName
 * @param {*} storeName tên store cần lấy dữ liệu
 * @returns trả về object hoặc array 
 */
function getStore(storeName) {
    var output; //undefined
    if (localStorage.getItem(storeName)) {
        output = JSON.parse(localStorage.getItem(storeName));

    }
    return output; //undefined
}

// XÓA NHÂN VIÊN
function xoaNhanVien(tknv) {
    var viTri = findI(mangNhanVien, tknv);
    if (viTri !== -1) {
        //xóa
        mangNhanVien.splice(viTri, 1);
        //lưu
        setStore(mangNhanVien, 'mangNhanVien');
        //render
        renderNhanVien(mangNhanVien);
    }
};

//Hàm sẽ chạy khi giao diện vừa load lên
window.onload = function() {
    //Lấy dữ liệu từ storage
    var content = getStore('mangNhanVien');
    //Nếu có dữ liệu trong storage thì xử lý in ra giao diện html cho table
    if (content) {
        mangNhanVien = content;
        var html = renderNhanVien(mangNhanVien);
        document.querySelector('tbody').innerHTML = html;
    }
}