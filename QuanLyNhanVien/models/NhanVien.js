function NhanVien(_tk, _hoTen, _email, _mk, _ngayLam, _luongCoBan, _chucVu, _gioLam) {
    this.tk = _tk;
    this.hoTen = _hoTen;
    this.email = _email;
    this.mk = _mk;
    this.ngayLam = _ngayLam;
    this.luongCoBan = _luongCoBan;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    this.tongLuong = function() {
        if (this.chucVu === "Sếp") {
            return this.luongCoBan * 3;
        } else if (this.chucVu === "Trưởng phòng") {
            return this.luongCoBan * 2;
        } else if (this.chucVu === "Nhân viên") {
            return this.luongCoBan * 1;
        }
    };
    this.xepLoai = function() {
        if (this.gioLam >= 192) {
            return " Nhân viên xuất sắc";
        } else if (this.gioLam >= 176) {
            return "Nhân viên giỏi";
        } else if (this.gioLam >= 160) {
            return " Nhân viên khá";
        } else if (this.gioLam < 160) {
            return "Nhân viên trung bình";
        }
    };

}

function findI(mangNhanVien, tknv) {
    return mangNhanVien.findIndex(function(item) {
        return item.tk == tknv;
    });
};