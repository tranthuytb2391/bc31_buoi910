/**
 * Hàm kiểm tra rỗng dựa vào giá trị người dùng nhập
 * @param {*} value value là giá trị người dùng nhập
 * @param {*} selectorError selector hiển thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi khi hiển thị
 * @returns trả về giá trị hợp lệ (true) hoặc không hợp lệ (false);
 */

// KIỂM TRA RỖNG
function kiemTraRong(value, selectorError, name) {
    if (value === "") {
        document.querySelector(selectorError).innerHTML = name + " không được để trống!";
        return false;
    }
    document.querySelector(selectorError).innerHTML = "";
    return true;
};

// KIỂM TRA ĐỘ DÀI
function kiemTraDoDai(value, selectorError, name, minLength, maxLength) {
    if (value.length < minLength || value.length > maxLength) {
        document.querySelector(selectorError).innerHTML = name + ' tối đa ' + minLength + ' đến ' + maxLength + ' ký số!';
        return false;
    }
    document.querySelector(selectorError).innerHTML = '';
    return true;
}

// KIỂM TRA KÍ TỰ

function kiemTraTatCaKyTu(value, selectorError, name) {
    var regexLetter = /^[A-Z a-z]+$/; //Nhập các kí tự a->z A->Z hoặc khoảng trống không bao gồm unicode
    if (!regexLetter.test(value)) { //test nếu ok false
        document.querySelector(selectorError).innerHTML = name + ' phải là chữ!';
        return false;
    }

    document.querySelector(selectorError).innerHTML = '';
    return true;
}

// KIỂM TRA EMAIL
function kiemTraEmail(value, selectorError, name) {
    var regexEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!regexEmail.test(value)) {
        document.querySelector(selectorError).innerHTML = name + ' không đúng định dạng !';
        return false;
    }

    document.querySelector(selectorError).innerHTML = '';
    return true;
}

// KIỂM TRA MẬT KHẨU
function kiemTraMatKhau(value, selectorError, name) {
    var regexNumber = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
    if (!regexNumber.test(value)) {
        document.querySelector(selectorError).innerHTML = name + ' từ 6-10 ký tự (chứa ít nhât 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)';
        return false;
    }

    document.querySelector(selectorError).innerHTML = '';
    return true;
}

// KIỂM TRA NGÀY
function kemTraDinhDangNgay(value, selectorError, name) {
    var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    if ((!date_regex.test(value))) {
        document.querySelector(selectorError).innerHTML = name + ' không đúng định dạng';
        return false;
    }

    return true;
}

// KIỂM TRA GIÁ TRỊ

function kiemTraGiaTri(value, selectorError, name, minValue, maxValue) {
    if (Number(value) < minValue || Number(value) > maxValue) {
        document.querySelector(selectorError).innerHTML = name + ' giá trị từ ' + minValue + ' đến ' + maxValue;
        return false;
    }
    document.querySelector(selectorError).innerHTML = '';
    return true;
}